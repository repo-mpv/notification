using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using IntegrateEvent.Type;
using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using IntegrateEvent.Interface;

namespace Demo
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IBus _bus;
        private Random _rnd = new Random();

        public Worker(IBus bus, ILogger<Worker> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var notification = new NotificationEventConstruct();
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var @event = notification.Construct(NotificationEventBuilderCollection.Get(GetRandomNotificationType()));
                    await _bus.Publish<INotificationEvent>(@event, stoppingToken);
                    var eventType = @event.GetType();
                    _logger.LogInformation($"{eventType}: {JsonSerializer.Serialize(@event, eventType)}");
                }
                catch (NotImplementedException e)
                {
                    _logger.LogWarning($"{e}");
                }
                catch (Exception e)
                {
                    _logger.LogError($"{e}");
                }
                await Task.Delay(7000, stoppingToken);
            }
        }
        public string GetRandomNotificationType()
        {
            var typeIndex = _rnd.Next(3);
            switch(typeIndex)
            {
                case 0: return NotificationType.Email;
                case 1: return NotificationType.Sms;
                default: return NotificationType.Unknown;
            }
        }
    }
}
