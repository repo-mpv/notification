﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegrateEvent.Constants.Notification;
using IntegrateEvent.Implementation.Notification;
using IntegrateEvent.Type;

namespace Demo
{
    public class NotificationEventConstruct
    {
        public NotificationEvent Construct(NotificationEventBuilder builder)
        {
            builder.SetType();
            builder.BuildParams();
            return builder.Event;
        }
    }
    public abstract class NotificationEventBuilder
    {
        public NotificationEvent Event { get; protected set; } = new NotificationEvent();
        public abstract void SetType();
        public abstract void BuildParams();
    }
    public class EmailNotificationEventBuilder : NotificationEventBuilder
    {
        protected class FakeEmailGenerator
        {
            private int _index = 0;
            public override string ToString() => $"email_{++_index}@fakeemail.ru";
        }
        protected FakeEmailGenerator _email { get; } = new FakeEmailGenerator();
        public override void SetType()
        {
            Event.Type = NotificationType.Email;
        }
        public override void BuildParams()
        {
            Event.Params = new Dictionary<string, object> {
                        { NotificationParamKey.To, _email.ToString() },
                        { NotificationParamKey.Message, "email_message" },
                        { NotificationParamKey.Subject, "Subject" }
                    };
        }
    }
    public class SmsNotificationEventBuilder : NotificationEventBuilder
    {
        public class FakePhoneNumberGenerator
        {
            private int _index = 0;
            public override string ToString() => $"{++_index}";

        }
        protected FakePhoneNumberGenerator _sms { get; } = new FakePhoneNumberGenerator();
        public override void SetType()
        {
            Event.Type = NotificationType.Sms;
        }
        public override void BuildParams()
        {
            Event.Params = new Dictionary<string, object> {
                        { NotificationParamKey.PhoneNumber, _sms.ToString() },
                        { NotificationParamKey.Message, "sms_message" }
                    };
        }
    }
    public static class NotificationEventBuilderCollection
    {
        public static NotificationEventBuilder Get(string type)
        {
            return _collection.FirstOrDefault(o => o.Key == type).Value ?? throw new NotImplementedException($"Unknown type: {type}");
        }
        private static IEnumerable<KeyValuePair<string, NotificationEventBuilder>> _collection = new KeyValuePair<string, NotificationEventBuilder>[]
        {
                new KeyValuePair<string, NotificationEventBuilder>(NotificationType.Email, new EmailNotificationEventBuilder()),
                new KeyValuePair<string, NotificationEventBuilder>(NotificationType.Sms, new SmsNotificationEventBuilder()),
        };
    }
}
