using BusService;
using Email.IntegrationEvents.EventHandling;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Email
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    RegisterEventBus(services, hostContext);
                });
        private static void RegisterEventBus(IServiceCollection services, HostBuilderContext context)
        {
            var config = context.Configuration.GetSection("MassTransit").Get<MassTransitConfig>() ?? new MassTransitConfig();
            services.AddMassTransit(x =>
            {
                x.AddConsumer<EmailNotificationEventHandler>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(rabbitMqBusFactoryConfigurator =>
                {
                    // configure health checks for this bus instance
                    rabbitMqBusFactoryConfigurator.Host(config.Host, config.VirtualHost, rabbitMqHostConfigurator =>
                    {

                        if (!string.IsNullOrEmpty(config.Username) && !string.IsNullOrEmpty(config.Password))
                        {
                            rabbitMqHostConfigurator.Username(config.Username);
                            rabbitMqHostConfigurator.Password(config.Password);
                        }
                    });
                    rabbitMqBusFactoryConfigurator.ReceiveEndpoint("email-notification", ep =>
                    {
                        ep.ConfigureConsumer<EmailNotificationEventHandler>(provider);
                    });
                }));
            });
            services.AddSingleton<IPublishEndpoint>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<IHostedService, BusService.BusService>();
        }
    }
}

