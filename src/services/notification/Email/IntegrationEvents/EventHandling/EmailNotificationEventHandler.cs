﻿using System.Text.Json;
using System.Threading.Tasks;
using IntegrateEvent.Interface;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Email.IntegrationEvents.EventHandling
{
    public class EmailNotificationEventHandler : IConsumer<IEmailNotificationEvent>
    {
        private readonly ILogger<EmailNotificationEventHandler> _logger;
        public EmailNotificationEventHandler(ILogger<EmailNotificationEventHandler> logger)
        {
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<IEmailNotificationEvent> context)
        {
            var @event = context.Message;

#warning TODO: SMTP client not implemented
            var eventType = @event.GetType();
            _logger?.LogInformation($"{eventType}: {JsonSerializer.Serialize(@event, eventType)}");
            await Task.Delay(100);
        }
    }
}
