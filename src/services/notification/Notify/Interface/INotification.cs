﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Notify.Interface
{
    public interface INotification
    {
        Task PublishAsync(
            IDictionary<string, object> @params
            , IBus bus
            , ILogger logger
            , CancellationToken cancellationToken = default);
    }
}
