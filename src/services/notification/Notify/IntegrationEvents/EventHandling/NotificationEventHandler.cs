﻿using System.Threading.Tasks;
using Notify.Implementation;
using IntegrateEvent.Constants.Notification;
using IntegrateEvent.Interface;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Notify.IntegrationEvents.EventHandling
{
    public class NotificationEventHandler : IConsumer<INotificationEvent>
    {
        private readonly IBus _bus;
        private readonly ILogger<NotificationEventHandler> _logger;
        public NotificationEventHandler(IBus bus
            , ILogger<NotificationEventHandler> logger)
        {
            _bus = bus;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<INotificationEvent> context)
        {
            var @event = context.Message;
            PrePublish(@event);
            var notification = NotificationEventCollection.Get(@event.Type);
            await notification.PublishAsync(@event.Params, _bus, _logger);
        }
        private void PrePublish(INotificationEvent @event)
        {
            @event.Params[NotificationParamKey.Id] = @event.Id;
            var message = MakeMessageTemplate(@event);
            if (message != null)
                @event.Params[NotificationParamKey.Message] = message;
        }
        private string MakeMessageTemplate(INotificationEvent @event)
        {
            string message = null;
#warning TODO: MakeMessageTemplate not implemented
            return message;
        }
    }
}
