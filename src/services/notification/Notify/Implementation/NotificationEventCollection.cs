﻿using System.Collections.Generic;
using System.Linq;
using Notify.Interface;
using Common;
using IntegrateEvent.Type;

namespace Notify.Implementation
{
    public static class NotificationEventCollection
    {
        public static INotification Get(string type)
        {
            return _notifications.FirstOrDefault(n => n.Key == type).Value ?? _notificationNull;
        }

        private static readonly INotification _notificationNull = ObjectFactory.Create<NotificationNull>();
        private static readonly IEnumerable<KeyValuePair<string, INotification>> _notifications = new KeyValuePair<string, INotification>[]
        {
            new KeyValuePair<string, INotification>(NotificationType.Email, ObjectFactory.Create<EmailNotification>()),
            new KeyValuePair<string, INotification>(NotificationType.Sms, ObjectFactory.Create<SmsNotification>())
        };
    }
}
