﻿using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Notify.Implementation.Builder;
using Notify.Interface;
using Common;
using IntegrateEvent.Interface;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Notify.Implementation
{
    public class Notification<TNotificationEventImpl, TNotificationBuilder>
        : INotification
        where TNotificationBuilder : NotificationEventBuilder<TNotificationEventImpl>, new()
        where TNotificationEventImpl : IIntegrateEvent, new()
    {
        private readonly NotificationEventConstruct _notification = new NotificationEventConstruct();
        
        public async Task PublishAsync(
              IDictionary<string, object> @params
            , IBus bus
            , ILogger logger
            , CancellationToken cancellationToken /*= default*/)
        {
            var @event = _notification.Construct(ObjectFactory.Create<TNotificationBuilder>(), @params);
            var eventType = @event.GetType();
            await bus?.Publish(@event, cancellationToken);
            logger?.LogInformation($"{eventType}: {JsonSerializer.Serialize(@event, eventType)}");
        }
    }
}
