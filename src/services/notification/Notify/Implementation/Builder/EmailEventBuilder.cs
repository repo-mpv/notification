﻿using IntegrateEvent.Implementation.Notification;

namespace Notify.Implementation.Builder
{
    public class EmailEventBuilder
        : NotificationEventBuilder<EmailNotificationEvent>
    { }
}
