﻿using IntegrateEvent.Implementation.Notification;

namespace Notify.Implementation.Builder
{
    public class SmsEventBuilder
        : NotificationEventBuilder<SmsNotificationEvent>
    { }
}
