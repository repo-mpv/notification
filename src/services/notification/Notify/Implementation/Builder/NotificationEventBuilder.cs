﻿using System.Collections.Generic;
using Common;
using IntegrateEvent.Interface;

namespace Notify.Implementation.Builder
{
    public class NotificationEventConstruct
    {
        public TNotificationEventImpl Construct<TNotificationEventImpl>(
            NotificationEventBuilder<TNotificationEventImpl> builder
            , IDictionary<string, object> @params)
             where TNotificationEventImpl : IIntegrateEvent, new()
        {
            builder.ResetNotificationEvent();
            builder.SetParams(@params);
            return builder.Event;
        }
    }
    public class NotificationEventBuilder<TNotificationEventImpl>
        where TNotificationEventImpl : IIntegrateEvent, new()
    {
        public TNotificationEventImpl Event { get; protected set; }

        public void ResetNotificationEvent()
        {
            Event = ObjectFactory.Create<TNotificationEventImpl>();
        }
        public void SetParams(IDictionary<string, object> @params)
        {
            var type = Event.GetType();
            var properties = type.GetProperties();
            foreach (var prop in properties)
            {
                object value;
                if (@params.TryGetValue(prop.Name, out value))
                {
                    prop.SetValue(Event, value);
                }
            }
        }
    }
}