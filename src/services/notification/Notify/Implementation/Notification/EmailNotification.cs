﻿using Notify.Implementation.Builder;
using IntegrateEvent.Implementation.Notification;
using IntegrateEvent.Interface;

namespace Notify.Implementation
{
    public class EmailNotification
        : Notification<EmailNotificationEvent, EmailEventBuilder>
    { }
}
