﻿using System.Text.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Notify.Interface;
using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading;

namespace Notify.Implementation
{
    public class NotificationNull : INotification
    {
        public async Task PublishAsync(
              IDictionary<string, object> @params
            , IBus bus
            , ILogger logger
            , CancellationToken cancellationToken = default)
        {
            logger?.LogWarning($"Unknown notification. @params: {JsonSerializer.Serialize(@params)}");
            await Task.Delay(100, cancellationToken);
        }
    }
}
