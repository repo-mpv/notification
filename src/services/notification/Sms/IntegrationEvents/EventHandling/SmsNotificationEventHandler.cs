﻿using System.Text.Json;
using System.Threading.Tasks;
using IntegrateEvent.Interface;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace Sms.IntegrationEvents.EventHandling
{
    public class SmsNotificationEventHandler : IConsumer<ISmsNotificationEvent>
    {
        private readonly ILogger<SmsNotificationEventHandler> _logger;
        public SmsNotificationEventHandler(ILogger<SmsNotificationEventHandler> logger)
        {
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<ISmsNotificationEvent> context)
        {
            var @event = context.Message;
#warning TODO: SMS client not implemented
            var eventType = @event.GetType();
            _logger?.LogInformation($"{eventType}: {JsonSerializer.Serialize(@event, eventType)}");
            await Task.Delay(100);
        }
    }
}
