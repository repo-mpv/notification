﻿namespace Common
{
    public static class ObjectFactory
    {
        public static TObject Create<TObject>()
            where TObject : new()
        {
            return new TObject();
        }
        public static TInterface Create<TInterface, TObject>()
            where TObject : TInterface, new()
        {
            return new TObject();
        }
    }
}
