﻿namespace IntegrateEvent.Type
{
    public class NotificationType
    {
        public const string Unknown = "unknown";
        public const string Email = "email";
        public const string Sms = "sms";
    }
}
