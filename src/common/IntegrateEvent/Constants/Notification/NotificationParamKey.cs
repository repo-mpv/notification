﻿namespace IntegrateEvent.Constants.Notification
{
    public class NotificationParamKey
    {
        public const string Id = "Id";
        public const string PhoneNumber = "PhoneNumber";
        public const string Message = "Message";
        public const string To = "To";
        public const string Subject = "Subject";
    }
}
