﻿using System;
using IntegrateEvent.Interface;

namespace IntegrateEvent.Implementation
{
    public class IntegrateEvent : IIntegrateEvent
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
    }
}
