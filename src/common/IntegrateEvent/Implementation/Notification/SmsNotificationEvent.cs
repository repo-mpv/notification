﻿using IntegrateEvent.Interface;
namespace IntegrateEvent.Implementation.Notification
{
    public class SmsNotificationEvent :
        IntegrateEvent, ISmsNotificationEvent
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}
