﻿using IntegrateEvent.Interface;
namespace IntegrateEvent.Implementation.Notification
{
    public class EmailNotificationEvent
        : IntegrateEvent, IEmailNotificationEvent
    {
        public string To { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
    }
}
