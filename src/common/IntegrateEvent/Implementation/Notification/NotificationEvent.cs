﻿using System.Collections.Generic;
using IntegrateEvent.Interface;

namespace IntegrateEvent.Implementation.Notification
{
    public class NotificationEvent :
        IntegrateEvent, INotificationEvent
    {
        public string Type { get; set; }
        public IDictionary<string, object> Params { get; set; }
    }
}
