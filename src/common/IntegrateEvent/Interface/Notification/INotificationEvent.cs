﻿using System.Collections.Generic;
namespace IntegrateEvent.Interface
{
    public interface INotificationEvent
        : IIntegrateEvent
    {
        /// <summary>
        /// sms, email
        /// </summary>
        string Type { get; }
        IDictionary<string, object> Params { get; }
    }
}
