﻿namespace IntegrateEvent.Interface
{
    public interface ISmsNotificationEvent
        : IIntegrateEvent
    {
        string PhoneNumber { get; }
        string Message { get; }
    }
}
