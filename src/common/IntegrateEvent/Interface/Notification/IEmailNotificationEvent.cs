﻿namespace IntegrateEvent.Interface
{
    public interface IEmailNotificationEvent
        : IIntegrateEvent
    {
        string To { get; }
        string Message { get; }
        string Subject { get; }
    }
}
