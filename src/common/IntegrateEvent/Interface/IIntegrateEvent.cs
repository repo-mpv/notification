﻿using System;
namespace IntegrateEvent.Interface
{
    public interface IIntegrateEvent
    {
        Guid Id { get; }
        DateTime CreationTime { get; }
    }
}
