﻿namespace BusService
{
    public class MassTransitConfig
    {
        public string Host { get; set; } = "localhost";
        public string VirtualHost { get; set; } = "/";
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
