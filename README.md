**Demo-project. Notification (Sms/Email)**

**.NET Core/MassTransit.RabbitMQ/Docker**

**BUILD**

docker-compose build

**Start**

docker-compose up

**Log**


```
demo_1      | info: Demo.Worker[0]
demo_1      |       IntegrateEvent.Implementation.Notification.NotificationEvent: {"Type":"email","Params":{"To":"email_1@fakeemail.ru","Message":"email_message","Subject":"Subject"},"Id":"84a60a4a-cb4e-4caa-8125-423f747054fd","CreationTime":"2020-09-29T14:23:31.3118331Z"}
notify_1    | info: Base.IntegrationEvents.EventHandling.NotificationEventHandler[0]
notify_1    |       IntegrateEvent.Implementation.Notification.EmailNotificationEvent: {"To":"email_1@fakeemail.ru","Message":"email_message","Subject":"Subject","Id":"84a60a4a-cb4e-4caa-8125-423f747054fd","CreationTime":"2020-09-29T14:23:46.8368784Z"}
email_1     | info: Email.IntegrationEvents.EventHandling.EmailNotificationEventHandler[0]
email_1     |       GreenPipes.DynamicInternal.IntegrateEvent.Interface.IEmailNotificationEvent: {"To":"email_1@fakeemail.ru","Message":"email_message","Subject":"Subject","Id":"84a60a4a-cb4e-4caa-8125-423f747054fd","CreationTime":"2020-09-29T14:23:46.8368784Z"}
```